{-# Language RankNTypes, KindSignatures #-}

module TeraReg.Strassen (qtStrass, fmapSubMatrixRect, strassQuads, transRect) where

import Numeric.LinearAlgebra.HMatrix 
import TeraReg.QuadTree

-- The quad size in words (8 bytes each word)
quadwords :: Int
quadwords =  512 * 1024

-- subMatrixFromRect :: 
--     Element a =>
--     Matrix a -> Rect -> Matrix a
-- subMatrixFromRect mm (Rect l t w h) =
--     subMatrix (t, l) (h, w) mm

fmapSubMatrixRect ::
    forall (f :: * -> *) a.
    (Element a, Functor f) =>
    Matrix a -> f Rect -> f (Matrix a)
fmapSubMatrixRect mm =
    let
        s (Rect l t w h) = subMatrix (t, l) (h, w) mm
    in
    fmap s

transRect :: Rect -> Rect
transRect (Rect l t w h) = Rect t l h w

transQuad :: Quad Rect -> Quad Rect
transQuad q =
    let (Quad a b c d) = fmap transRect q
    in
        Quad a c b d
        
-- \mathbf{M}_{1} := (\mathbf{A}_{1,1} + \mathbf{A}_{2,2}) (\mathbf{B}_{1,1} + \mathbf{B}_{2,2})
-- \mathbf{M}_{2} := (\mathbf{A}_{2,1} + \mathbf{A}_{2,2}) \mathbf{B}_{1,1}
-- \mathbf{M}_{3} := \mathbf{A}_{1,1} (\mathbf{B}_{1,2} - \mathbf{B}_{2,2})
-- \mathbf{M}_{4} := \mathbf{A}_{2,2} (\mathbf{B}_{2,1} - \mathbf{B}_{1,1})
-- \mathbf{M}_{5} := (\mathbf{A}_{1,1} + \mathbf{A}_{1,2}) \mathbf{B}_{2,2}
-- \mathbf{M}_{6} := (\mathbf{A}_{2,1} - \mathbf{A}_{1,1}) (\mathbf{B}_{1,1} + \mathbf{B}_{1,2})
-- \mathbf{M}_{7} := (\mathbf{A}_{1,2} - \mathbf{A}_{2,2}) (\mathbf{B}_{2,1} + \mathbf{B}_{2,2})
--
-- only using 7 multiplications (one for each Mk) instead of 8. We may
-- now express the Ci,j in terms of Mk, like this:
--
-- 
-- \mathbf{C}_{1,1} = \mathbf{M}_{1} + \mathbf{M}_{4} - \mathbf{M}_{5} + \mathbf{M}_{7}
-- \mathbf{C}_{1,2} = \mathbf{M}_{3} + \mathbf{M}_{5}
-- \mathbf{C}_{2,1} = \mathbf{M}_{2} + \mathbf{M}_{4}
-- \mathbf{C}_{2,2} = \mathbf{M}_{1} - \mathbf{M}_{2} + \mathbf{M}_{3} + \mathbf{M}_{6}
strassQuads ::
    Quad (Matrix Double) ->
    Quad (Matrix Double) ->
    Matrix Double
strassQuads (Quad a11 a12 a21 a22) (Quad b11 b12 b21 b22) =
    let
        m1 = (a11 + a22) `mul` (b11 + b22)
        m2 = (a21 + a22) `mul` b11
        m3 = a11 `mul` (b12 - b22)
        m4 = a22 `mul` (b21 - b11)
        m5 = (a11 + a12) `mul` b22
        m6 = (a21 - a11) `mul` (b11 + b12)
        m7 = (a12 - a22) `mul` (b21 + b22)
        c11 = m1 + m4 - m5 + m7
        c12 = m3 + m5
        c21 = m2 + m4
        c22 = m1 - m2 + m3 + m6
    in
        (c11 ||| c12) === (c21 ||| c22)

qtStrass :: Matrix Double -> Matrix Double -> Matrix Double
qtStrass m n =
    let
        (hm, wm) = size m
        strass :: QuadTree (Quad Rect) -> QuadTree (Quad Rect) -> Matrix Double
        strass (Leaf p) (Leaf q) =
            let
                mp = fmapSubMatrixRect m p
                mq = fmapSubMatrixRect n q
            in
                strassQuads mp mq
                
        strass (Node a11 a12 a21 a22) (Node b11 b12 b21 b22) =
            let
                a = strass a11 b11
                b = strass a12 b21

                c = strass a11 b12
                d = strass a12 b22

                e = strass a21 b12
                f = strass a22 b22

                g = strass a21 b12
                h = strass a22 b22

                c11 = a + b
                c12 = c + d
                c21 = e + f
                c22 = g + h
            in
                (c11 ||| c12 ) === (c21 ||| c22)

        z0 = zorder (Rect 0 0 wm hm) quadwords
        zt = fmap transQuad z0
    in
        strass z0 zt
