{-# Language RankNTypes, KindSignatures #-}

module TeraReg.MatrixMult where

import Control.Concurrent.Async (mapConcurrently)
import Numeric.LinearAlgebra.HMatrix 

thresh :: Int
thresh = 5000

strassen :: Matrix Double -> Matrix Double -> Matrix Double
strassen m n = strassen'' m n

strassen' :: Matrix Double -> Matrix Double -> IO (Matrix Double)
strassen' m n =
    if (x_h m < thresh)
    then return $ m `mul` n
    else strass m n
    where
        x_h = fst . size
        x_w = snd . size
        r11 x = subMatrix (0, 0) ((x_h x) `div` 2, (x_w x) `div` 2) x
        r12 x = subMatrix (0, (x_w x) `div` 2 + 1) ((x_h x `div` 2), x_w x - ((x_w x) `div` 2)) x
        r21 x = subMatrix ((x_h x) `div` 2 + 1, 0) (x_h x - (x_h x) `div` 2, (x_w x) `div` 2) x
        r22 x = subMatrix ((x_h x) `div` 2 + 1, (x_w x) `div` 2 + 1) (x_h x - (x_h x) `div` 2, x_w x - ((x_w x) `div` 2)) x

        strass mm nn = do
            [a, b, c, d, e, f, g, h] <- mapConcurrently (\(m0, m1) -> strassen' m0 m1) [
                ((r11 mm), (r11 nn)),
                ((r12 mm), (r21 nn)),

                ((r11 mm), (r12 nn)),
                ((r12 mm), (r22 nn)),

                ((r21 mm), (r12 nn)),
                ((r22 mm), (r22 nn)),

                ((r21 mm), (r12 nn)),
                ((r22 mm), (r22 nn))]

            let c11 = a + b
            let c12 = c + d
            let c21 = e + f
            let c22 = g + h

            return ((c11 ||| c12) === (c21 ||| c22))

strassen'' :: Matrix Double -> Matrix Double -> Matrix Double
strassen'' m n =
    if (x_h m < thresh)
    then m `mul` n
    else strass m n
    where
        x_h = fst . size
        x_w = snd . size
        r11 x = subMatrix (0, 0) ((x_h x) `div` 2, (x_w x) `div` 2) x
        r12 x = subMatrix (0, (x_w x) `div` 2 + 1) ((x_h x `div` 2), x_w x - ((x_w x) `div` 2)) x
        r21 x = subMatrix ((x_h x) `div` 2 + 1, 0) (x_h x - (x_h x) `div` 2, (x_w x) `div` 2) x
        r22 x = subMatrix ((x_h x) `div` 2 + 1, (x_w x) `div` 2 + 1) (x_h x - (x_h x) `div` 2, x_w x - ((x_w x) `div` 2)) x

        strass mm nn =
            let
                [a, b, c, d, e, f, g, h] = map (\(m0, m1) -> strassen'' m0 m1) [
                    ((r11 mm), (r11 nn)),
                    ((r12 mm), (r21 nn)),

                    ((r11 mm), (r12 nn)),
                    ((r12 mm), (r22 nn)),

                    ((r21 mm), (r12 nn)),
                    ((r22 mm), (r22 nn)),

                    ((r21 mm), (r12 nn)),
                    ((r22 mm), (r22 nn))]

                c11 = a + b
                c12 = c + d
                c21 = e + f
                c22 = g + h
            in

                ((c11 ||| c12) === (c21 ||| c22))
