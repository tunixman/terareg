{-# Language OverloadedStrings #-}

import Criterion.Main hiding (defaultConfig)

import TeraReg.DataGen
import TeraReg.LinReg
import TeraReg.MatrixMult
import TeraReg.Strassen
import Numeric.LinearAlgebra.HMatrix 

main :: IO ()
main = do
    let (p, r) = generateData $ defaultConfig (128 * 1024) 64

    let _ = toRows p
    
    defaultMain [
        bgroup "LinReg" [
                bench "fast_ols pure" $ nf (fast_ols p) r,
                bench "fast_ols io" $ nfIO $ fast_ols_io p r,
                bench "qtstrass_ols" $ nf (qtstrass_ols p) r
                ],
        bgroup "MatMult" [
            bench "strassen" $ nf (strassen (tr p)) p,
            bench "strassen'" $ nfIO $ strassen' (tr p) p,
            bench "qtStrass" $ nf (qtStrass (tr p)) p
            ]
        ]
